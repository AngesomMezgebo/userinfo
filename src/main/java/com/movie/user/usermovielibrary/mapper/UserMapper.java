package com.movie.user.usermovielibrary.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.movie.user.usermovielibrary.model.User;





public class UserMapper implements RowMapper<User>  {
	private final static Logger LOGGER = LoggerFactory.getLogger(UserMapper.class);
	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException{
		User user = new User();
		user.setUse_id(rs.getInt("user_id")==0?0: rs.getInt("user_id"));
		user.setFirst_name(rs.getString("first_name")==null?null : rs.getString("first_name"));
		user.setLast_name(rs.getString("last_name")==null?null : rs.getString("last_name"));
		
		user.setEmail(rs.getString("email")==null?null : rs.getString("email"));
		user.setMovie_id(rs.getInt("movie_id")==0?0: rs.getInt("movie_id"));
		LOGGER.info("IN MAPPER: "+user);
		
		return user;
	}

}
