package com.movie.user.usermovielibrary.model;

public class User {
	private int use_id;
	private String first_name;
	private String last_name;
	private String email;
	private int movie_id;

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(int use_id, String first_name, String last_name, String email,int movie_id) {
		super();
		this.use_id = use_id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.movie_id=movie_id;
	}
	public int getUse_id() {
		return use_id;
	}
	public void setUse_id(int use_id) {
		this.use_id = use_id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getMovie_id() {
		return movie_id;
	}
	public void setMovie_id(int movie_id) {
		this.movie_id = movie_id;
	}
	@Override
	public String toString() {
		return "User [use_id=" + use_id + ", first_name=" + first_name + ", last_name=" + last_name + ", email=" + email
				+ ", movie_id=" + movie_id + "]";
	}
	
	

}
