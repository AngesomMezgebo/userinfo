package com.movie.user.usermovielibrary.service;

import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import com.movie.user.usermovielibrary.dao.UserDao;
import com.movie.user.usermovielibrary.model.User;


@Component
public class UserService {
	private final static Logger LOGGER = LoggerFactory.getLogger(UserService.class);
	@Autowired
	private UserDao userDao;
	
	public List<User> retrieveUsers() throws SQLException{
		LOGGER.info("In Service: "+User.class);
		
		return userDao.retrieveAll();
	}

	public User addUser(User user) {
		
		userDao.CreateUser(user);
		LOGGER.info("In Service: "+user.getUse_id() + " , "+user.getFirst_name());
		// TODO Auto-generated method stub
		return user;
	}

	public User getUser(int user_id) throws SQLException {
		User user = userDao.findUser(user_id);
		LOGGER.info("In getUser() - > Service: "+user.getFirst_name() +" , "+user.getUse_id());
		return user;
	}
	
	
	

}
