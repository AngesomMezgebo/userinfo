package com.movie.user.usermovielibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class UserMovieLibraryApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserMovieLibraryApplication.class, args);
	}

}

