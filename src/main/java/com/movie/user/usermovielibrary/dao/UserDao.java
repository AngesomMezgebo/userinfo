package com.movie.user.usermovielibrary.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.movie.user.usermovielibrary.mapper.UserMapper;
import com.movie.user.usermovielibrary.model.User;






@Repository
public class UserDao {
	private final static Logger LOGGER = LoggerFactory.getLogger(UserDao.class);
	

	
	@Value("${insertUser}")
	private String insertUser;
	
	@Value("${Userssql}")
	private String UsersSql;
	
	@Value("${get_user}")
	private String get_user;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
			
	
	public  void CreateUser(final User user) {
		
		LOGGER.info("Inserting into person table " + user.getFirst_name());

		jdbcTemplate.update(new PreparedStatementCreator() {		

			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {

				PreparedStatement ps = con.prepareStatement(insertUser);
				LOGGER.info("Inserting into person table " + insertUser);
				ps.setString(1, user.getFirst_name());
				ps.setString(2, user.getLast_name());
				ps.setString(3,  user.getEmail());
				ps.setInt(4,  user.getMovie_id());


				return ps;
			}
		});
		/*allMovieUsers.add(new User (1,"Angesom","Amare","test@gmail.com"));
		allMovieUsers.add(new User (1,"Bereket","Frezghi","test@gmail.com"));
		allMovieUsers.add(new User (1,"Danait","Zeru","test@gmail.com"));
		allMovieUsers.add(new User (1,"test","Amare","test@gmail.com"));*/
		
	}
	
	public List<User> retrieveAll() throws SQLException{
		//CreateUser();
		LOGGER.info("sql: "+UsersSql);
		List<User> allMovieUsers = jdbcTemplate.query(UsersSql,new UserMapper());
		if(allMovieUsers==null||allMovieUsers.isEmpty()) {
			
			LOGGER.info("User not found " );
			return null;
		}
		LOGGER.info("in Dao: "+allMovieUsers);
		//allMovieUsers = jdbcTemplate.update(UsersSql,new UserMapper());
		return allMovieUsers;
	}

	public User findUser(int user_id) throws SQLException {
		User user=jdbcTemplate.queryForObject(get_user,new UserMapper(),user_id);
		/*List<User> allMovieUsers = jdbcTemplate.query(UsersSql,new UserMapper());
		for(User user:allMovieUsers) {
			if(user.getUse_id()==user_id) {
				return user;
			}
		}*/
		return user;
	}
}
