package com.movie.user.usermovielibrary.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.movie.user.usermovielibrary.model.User;
import com.movie.user.usermovielibrary.service.UserService;

@RestController
public class UserController {
	private final static Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	@Autowired
	private UserService userService;
	
	
	

		
	@GetMapping(path="/users")	
	public List<User> retrieveUsers() throws SQLException{
		
		LOGGER.info("In Controller: ");
		//List<User> alluser = userService.retrieveUsers();
		return userService.retrieveUsers();
	}	
	
	@PostMapping("/addUser")
	public User addUser(@Valid @RequestBody User user) {
		LOGGER.info("In Controller: "+user.getFirst_name());
		User adduser = userService.addUser(user);
		
		return adduser;
	}
	
	@GetMapping("/user/{user_id}")
	public User getUser(@PathVariable int user_id) throws SQLException {
		User user = userService.getUser(user_id);
	
		LOGGER.info("In getUser() - > Controller: "+user);
		return user;
		
		
		
		
		//return user;
	}
}
